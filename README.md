# key-consulting-technical-test
This project was created to demonstrate skills to Key consulting company to create a Springboot API 
by evaluating the following points:
* separation between business and technique using the model Controller-Service-Repository
* make a documentation of the API
* compliance of REST standard

## How to run this project

### Gradle 
Open a terminal at the project location and paste the following line.

`gradlew bootRun`

### Docker
Make sure that docker is running and open a terminal at the project location and paste the following line.

`docker build -t key-consulting-technical-test . && docker run -p 8080 -e SERVER_PORT=XXXXXX -e DATABASE_URL=XXXXX -e DATABASE_NAME=XXXXXX -e DATABASE_PORT=5432 -e DATABASE_USER_NAME=XXXXXX -e DATABASE_USER_PWD=XXXXXX key-consulting-technical-test`

### Docker-compose
The easiest way to launch the application and the database.

Make sure that docker is running and open a terminal at the project location and paste the following line.

`docker-compose -f local-deployment/docker-compose.yml up --build`

## How to test this project

### Run the test
Open a terminal at the project location and paste the following line.

`gradlew test`

### Run the test coverage
Open a terminal at the project location and paste the following line.

`gradlew jacocoTestCoverageVerification`

## How to get more informations about the App

### Actuator
Actuator let you know somes informations about the application like:
* his health
* his metrics, etc

To access Actuator, launch the project and open on a browser the adress `http://localhost:8081` and add on of the path presented bellow
#### Two endpoint are exposed without security
* /actuator/health
* /actuator/info

##### The rest of the endpoints are secured by the default couple 'admin/admin'. You can test the security by calling the endpoint:
* /actuator (to see all of the endpoints)
* /actuator/metrics

### Swagger
Swagger let you have a documentation of the API implemented of the application

To access swagger, launch the project and go to `http://localhost:8081/swagger-ui.html`