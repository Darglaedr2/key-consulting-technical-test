CREATE DATABASE "my-awesome-project";
\connect my-awesome-project

DROP TABLE IF EXISTS "task";

DROP SEQUENCE IF EXISTS task_id_seq;

CREATE SEQUENCE task_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."task" (
    "id" integer DEFAULT nextval('task_id_seq') NOT NULL,
    "label" character varying(50) NOT NULL,
    "status" integer NOT NULL
);