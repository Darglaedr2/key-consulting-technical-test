package com.dargleadr.keyconsultingtechnicaltest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "task")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(name = "id", position = 1, example = "1")
    private int id;

    @ApiModelProperty(name = "label", position = 2, example = "My task")
    private String label;

    @ApiModelProperty(name = "status", position = 3, example = "1")
    private int status;

    public Task(String label, int status) {
        this.label = label;
        this.status = status;
    }
}
