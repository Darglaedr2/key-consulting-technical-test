package com.dargleadr.keyconsultingtechnicaltest;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TasksRepository extends CrudRepository<Task, Integer> {
    List<Task> findAll();
    Task findById(int id);
    List<Task> findByStatus(int status);
    List<Task> findByStatusNot(int status);
}
