package com.dargleadr.keyconsultingtechnicaltest;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path = "/tasks")
@Api("API for managing tasks.")
public class TasksController {

    @Autowired
    private ITasksServices iTasksServices;

    @ApiOperation(value = "Get a list of Tasks according to the query")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successfully get tasks according to the query", response = Task.class, responseContainer = "List"),
            @ApiResponse(code = 401, message = "Return in case of the status and status! is asked at the same time")
    })
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getTasks(@RequestParam Optional<Integer> status, @RequestParam(name = "status!") Optional<Integer> statusNot) {
        List<Task> tasks;
        if(status.isPresent() && statusNot.isPresent()) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        } else if(status.isPresent()) {
            tasks = iTasksServices.findByStatus(status.get());
        } else if(statusNot.isPresent()) {
            tasks = iTasksServices.findByStatusNot(statusNot.get());
        } else if(status.isEmpty() && statusNot.isEmpty()) {
            tasks = iTasksServices.findAll();
        } else {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity(tasks, null, HttpStatus.OK);
    }

    @ApiOperation(value = "Get a specific task")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successfully get task", response = Task.class),
            @ApiResponse(code = 204, message = "No task with the id found")
    })
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getTaskById(@PathVariable(required = true) int id) {

        Task task = iTasksServices.findOne(id);
        if(task == null) {
            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity(task, null, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Create task")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Task successfully created", response = Task.class),
            @ApiResponse(code = 401, message = "An id of the task was send in the body"),
            @ApiResponse(code = 500, message = "An unknown error occurred")
    })
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity postTasks(@RequestBody Task task) {
        if(task.getId() != 0) {
            return new ResponseEntity(null, HttpStatus.UNAUTHORIZED);
        }
        Task newTask = iTasksServices.createTask(task);
        if(newTask == null) {
            return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity(newTask, null, HttpStatus.CREATED);
        }
    }

    @ApiOperation(value = "Update task")
    @ApiResponses({
            @ApiResponse(code = 202, message = "Task successfully updated", response = Task.class),
            @ApiResponse(code = 401, message = "The id on the path and in the task are different")
    })
    @RequestMapping(method = {RequestMethod.PUT}, value = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity putTasks(@PathVariable(required = true) int id, @RequestBody Task task) {
        Task newTask = iTasksServices.updateTask(id, task);
        if(newTask == null) {
            return new ResponseEntity(null, HttpStatus.UNAUTHORIZED);
        } else {
            return new ResponseEntity(newTask, null, HttpStatus.ACCEPTED);
        }
    }
}
