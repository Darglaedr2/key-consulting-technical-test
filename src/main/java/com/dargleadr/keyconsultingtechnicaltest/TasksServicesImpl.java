package com.dargleadr.keyconsultingtechnicaltest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TasksServicesImpl implements ITasksServices {
    @Autowired
    private TasksRepository repository;

    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Override
    public Task findOne(int id) {
        return repository.findById(id);
    }

    @Override
    public Task updateTask(int id, Task task) {
        if(task.getId() > 0 && id != task.getId()) {
            return null;
        } else {
            task.setId(id);
            return repository.save(task);
        }
    }

    @Override
    public List<Task> findByStatus(int status) {
        return repository.findByStatus(status);
    }

    @Override
    public List<Task> findByStatusNot(int status) {
        return repository.findByStatusNot(status);
    }

    @Override
    public Task createTask(Task task) {
        return repository.save(task);
    }
}
