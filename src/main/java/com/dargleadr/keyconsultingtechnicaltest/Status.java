package com.dargleadr.keyconsultingtechnicaltest;

public final class Status {
    public static final int ASSIGNED = 1;
    public static final int NOT_ASSIGNED = 2;
    public static final int CLOSED = 3;
}
