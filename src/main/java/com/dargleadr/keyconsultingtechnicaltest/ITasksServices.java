package com.dargleadr.keyconsultingtechnicaltest;

import java.util.List;

public interface ITasksServices {
    List<Task> findAll();
    Task findOne(int id);
    Task updateTask(int id, Task task);
    List<Task> findByStatus(int status);
    List<Task> findByStatusNot(int status);
    Task createTask(Task task);
}
