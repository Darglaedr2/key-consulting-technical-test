package com.dargleadr.keyconsultingtechnicaltest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TasksServicesImpl.class})
public class TasksServiceTests {

    @Autowired
    private TasksServicesImpl services;

    @MockBean
    private TasksRepository repository;

    public final Task task1 = new Task(1, "task1", Status.ASSIGNED);
    public final Task task2 = new Task(2, "task2", Status.NOT_ASSIGNED);
    public final Task task3 = new Task(3, "task3", Status.CLOSED);
    public final Task task4 = new Task(4, "task4", Status.ASSIGNED);
    public final List<Task> tasks = Arrays.asList(task1, task2, task3, task4);

    @Test
    public void testFindAll() throws Exception {
        Mockito
                .when(repository.findAll())
                .thenReturn(tasks);

        Assertions.assertArrayEquals(tasks.toArray(), services.findAll().toArray());
    }

    @Test
    public void testFindOneFound() throws Exception {
        Mockito
                .when(repository.findById(task1.getId()))
                .thenReturn(task1);

        Assertions.assertEquals(task1, services.findOne(task1.getId()));
    }

    @Test
    public void testFindOneNotFound() throws Exception {
        Mockito
                .when(repository.findById(50))
                .thenReturn(null);

        Assertions.assertNull(services.findOne(50));
    }

    @Test
    public void testFindByStatus() throws Exception {
        var assignedTasks = Arrays.asList(task1, task4);
        var notAssignedTasks = Collections.singletonList(task2);
        var closedTasks = Collections.singletonList(task3);
        Mockito
                .when(repository.findByStatus(Status.ASSIGNED))
                .thenReturn(assignedTasks);
        Mockito
                .when(repository.findByStatus(Status.NOT_ASSIGNED))
                .thenReturn(notAssignedTasks);
        Mockito
                .when(repository.findByStatus(Status.CLOSED))
                .thenReturn(closedTasks);

        Assertions.assertEquals(assignedTasks, services.findByStatus(Status.ASSIGNED));
        Assertions.assertEquals(notAssignedTasks, services.findByStatus(Status.NOT_ASSIGNED));
        Assertions.assertEquals(closedTasks, services.findByStatus(Status.CLOSED));
    }

    @Test
    public void testFindByStatusNot() throws Exception {
        var notAssignedTasks = Arrays.asList(task2, task3);
        var notNotAssignedTasks = Arrays.asList(task1, task3, task4);
        var notClosedTasks = Arrays.asList(task1, task2, task4);
        Mockito
                .when(repository.findByStatusNot(Status.ASSIGNED))
                .thenReturn(notAssignedTasks);
        Mockito
                .when(repository.findByStatusNot(Status.NOT_ASSIGNED))
                .thenReturn(notNotAssignedTasks);
        Mockito
                .when(repository.findByStatusNot(Status.CLOSED))
                .thenReturn(notClosedTasks);

        Assertions.assertEquals(notAssignedTasks, services.findByStatusNot(Status.ASSIGNED));
        Assertions.assertEquals(notNotAssignedTasks, services.findByStatusNot(Status.NOT_ASSIGNED));
        Assertions.assertEquals(notClosedTasks, services.findByStatusNot(Status.CLOSED));
    }

    @Test
    public void testCreate() throws Exception {
        var newTask = new Task("new tasks", Status.NOT_ASSIGNED);
        var savedNewTask = new Task(10, newTask.getLabel(), newTask.getStatus());

        Mockito
                .when(repository.save(newTask))
                .thenReturn(savedNewTask);

        Assertions.assertEquals(savedNewTask, services.createTask(newTask));
    }

    @Test
    public void testUpdate() throws Exception {
        var newTask = new Task(task1.getId(), task1.getLabel(), Status.CLOSED);

        Mockito
                .when(repository.save(newTask))
                .thenReturn(newTask);

        Assertions.assertEquals(newTask, services.updateTask(newTask.getId(), newTask));
    }

    @Test
    public void testUpdateError() throws Exception {
        var newTask = new Task(task1.getId() + 1, task1.getLabel(), Status.CLOSED);

        Mockito
                .when(repository.save(newTask))
                .thenReturn(newTask);

        Assertions.assertNull(services.updateTask(task1.getId(), newTask));
    }

}
