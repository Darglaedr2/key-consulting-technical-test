package com.dargleadr.keyconsultingtechnicaltest;

import com.google.gson.Gson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static com.dargleadr.keyconsultingtechnicaltest.Utils.taskToJsonObject;
import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TasksController.class)
public class TasksControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    TasksServicesImpl tasksServices;

    public final Task task1 = new Task(1, "task1", Status.ASSIGNED);
    public final Task task2 = new Task(2, "task2", Status.NOT_ASSIGNED);
    public final Task task3 = new Task(3, "task3", Status.CLOSED);
    public final Task task4 = new Task(4, "task4", Status.ASSIGNED);

    @BeforeEach
    public void beforeEach() {
        List<Task> t = Arrays.asList(task1, task2, task3, task4);
        given(tasksServices.findAll()).willReturn(t);

        given(tasksServices.findOne(task1.getId())).willReturn(task1);
        given(tasksServices.findOne(task2.getId())).willReturn(task2);
        given(tasksServices.findOne(task3.getId())).willReturn(task3);
        given(tasksServices.findOne(task4.getId())).willReturn(task4);

        given(tasksServices.findByStatus(Status.ASSIGNED)).willReturn(Arrays.asList(task1, task4));
        given(tasksServices.findByStatus(Status.NOT_ASSIGNED)).willReturn(Arrays.asList(task2));
        given(tasksServices.findByStatus(Status.CLOSED)).willReturn(Arrays.asList(task3));

        given(tasksServices.findByStatusNot(Status.CLOSED)).willReturn(Arrays.asList(task1, task2, task4));
        given(tasksServices.findByStatusNot(Status.ASSIGNED)).willReturn(Arrays.asList(task2, task3));
        given(tasksServices.findByStatusNot(Status.NOT_ASSIGNED)).willReturn(Arrays.asList(task1, task3, task4));
    }

    @Test
    public void testGetTasks() throws Exception {
        mockMvc.perform(get("/tasks"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].label").value(containsInAnyOrder(task1.getLabel(), task2.getLabel(), task3.getLabel(), task4.getLabel())))
                .andExpect(jsonPath("$[*].status").value(containsInRelativeOrder(task1.getStatus(), task2.getStatus(), task3.getStatus(), task4.getStatus())));
    }

    @Test
    public void testGetTask() throws Exception {
        mockMvc.perform(get("/tasks/" + task1.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.label").value(task1.getLabel()))
                .andExpect(jsonPath("$.status").value(task1.getStatus()));
    }

    @Test
    public void testGetTaskNotExist() throws Exception {
        mockMvc.perform(get("/tasks/30"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testGetTaskNotClosed() throws Exception {
        mockMvc.perform(get("/tasks?status!=" + Status.CLOSED))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].label").value(containsInAnyOrder(task1.getLabel(), task2.getLabel(), task4.getLabel())))
                .andExpect(jsonPath("$[*].status").value(containsInAnyOrder(task1.getStatus(), task2.getStatus(), task4.getStatus())));
    }

    @Test
    public void testGetTaskClosed() throws Exception {
        mockMvc.perform(get("/tasks?status=" + Status.CLOSED))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].label").value(containsInAnyOrder(task3.getLabel())))
                .andExpect(jsonPath("$[*].status").value(containsInAnyOrder(task3.getStatus())));
    }

    @Test
    public void testPostTask() throws Exception {
        Task newTask = new Task("task5", Status.NOT_ASSIGNED);
        Task returnedTask = new Task(new Random().nextInt() + 10, newTask.getLabel(), newTask.getStatus());
        given(tasksServices.createTask(newTask)).willReturn(returnedTask);

        mockMvc.perform(post("/tasks")
                .content(taskToJsonObject(newTask).toString())
                .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id").value(isA(Integer.class)));
    }

    @Test
    public void testPutTask() throws Exception {
        Task updatedTask = new Task(task4.getId(), task4.getLabel(), task4.getStatus());
        updatedTask.setStatus(Status.CLOSED);
        given(tasksServices.updateTask(updatedTask.getId(), updatedTask)).willReturn(updatedTask);

        mockMvc.perform(put("/tasks/" + updatedTask.getId())
                .content(new Gson().toJson(updatedTask))
                .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isAccepted())
            .andExpect(jsonPath("$.id").value(updatedTask.getId()))
            .andExpect(jsonPath("$.label").value(updatedTask.getLabel()))
            .andExpect(jsonPath("$.status").value(updatedTask.getStatus()));
    }
}
