package com.dargleadr.keyconsultingtechnicaltest;

import com.google.gson.JsonObject;

public final class Utils {

    static JsonObject taskToJsonObject(Task task) {
        JsonObject json = new JsonObject();
        json.addProperty("label", task.getLabel());
        json.addProperty("status", task.getStatus());
        return json;
    }
}
