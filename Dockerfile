# STEP 1 build
FROM gradle:7.1.0-jdk11 as build-stage
WORKDIR /app
COPY . ./
RUN gradle assemble

# STEP 2 deploy
FROM gcr.io/distroless/java:11
COPY --from=build-stage /app/build/libs/main.jar /app/main.jar
WORKDIR /app
ENV SERVER_PORT=8081
ENTRYPOINT ["java","-jar","main.jar"]
